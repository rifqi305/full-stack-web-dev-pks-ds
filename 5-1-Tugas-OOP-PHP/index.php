<?php

trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahkaki;
    public $keahlian;

    public function atraksi()
    {
        echo " {$this->nama} sedang {$this->keahlian}";
    }
}


abstract class Fight {
    use hewan;
    public $attackpower;
    public $defencepower;

    public function serang($hewan)
    {
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        echo "<br>";
        $hewan ->diserang($this);
    }

    public function diserang ($hewan)
    {
        echo "{$this->nama} sedang diserang {$hewan->nama}";
    $this->darah = $this->darah - ($hewan->attackpower / $this->defencepower);
    }
    protected function getInfo()
    {
        echo"<br>";
        echo " Nama : {$this->nama}";
        echo "<br>";
        echo " Jumlah kaki: {$this->jumlahkaki}";
        echo "<br>";
        echo  "keahlian :{$this->keahlian}";
        echo  "Darah :{$this->darah}";
        echo "<br>";
        echo  "Attack Power :{$this->attackpower}";
        echo "<br>";
        echo  "defence power :{$this->defencepower}";  
    }

    abstract public function getInfoHewan();
}
class Elang extends Fight{

    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahkaki = 2;
        $this->keahlian = "tebang tinggi";
        $this->attackpower = 10;
        $this->defencepower = 5;
    }
       public function getInfoHewan(){
        echo " Jenis Hewan : Elang";
        $this->getInfo();
        
    }
}

class Harimau extends Fight{
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahkaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackpower = 7;
        $this->defencepower = 8;
    }
    public function getInfoHewan(){
        echo " Jenis Hewan : Harimau";
        
        $this->getInfo();
    }
}
class spasi{
    public static function tampilkan()
    {
        echo "<br>";
        echo "=============";
        echo "<br>";
    }
}

$harimau = new Harimau ("Harimau sumatra");
$harimau->getInfoHewan();
spasi::tampilkan();
$elang = new Elang("Elang Jawa");
$elang->getInfoHewan();
spasi::tampilkan();
$harimau->serang($elang);
spasi::tampilkan();
$elang->getInfoHewan();
spasi::tampilkan();

$elang->serang($harimau);
spasi::tampilkan();
$harimau->getInfoHewan();

?>

